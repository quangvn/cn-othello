#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

#include "global_limits.h"
#include "cn_player.h"
#include "othello_eval.h"

#define sqrt(x) pow(x,0.5)
#define sqr(x) (x*x)

using namespace std;

int CNPlayer::node_visited = 0;
int CNPlayer::node_terminal = 0;

CNPlayer::CNPlayer() {
  search_depth = 4;
  eval_function = OthelloEval::simple_eval;
  LOG_FILE = "log\\ev_4.txt";
}

CNPlayer::CNPlayer(int depth, othello_eval e) {
  search_depth = depth;
  eval_function = e;
  LOG_FILE = "log\\ev_1.txt";
}

move_pair_t CNPlayer::selectMove(OthelloGame *game) {
  node_visited = 0;
  node_terminal = 0;
  OthelloGame::game_inst_created = 0;
  OthelloGame::game_inst_destroyed = 0;

  MinimaxNode::node_inst_reused = 0;

  MinimaxNode* root = MinimaxNode::create(game);

  negamax(root, game->clone(), search_depth);

  MinimaxNode* node;
  float adjusted_eval;
  auto children = root->get_children();

  for (auto & child : (*children)) {
    node = child.second;
    node->set_eval(compute_cn_mean_eval(node));
  }

  move_pair_t move = root->getBestMove();
  delete root;

  return move;
}

float CNPlayer::compute_cn_adjustment(MinimaxNode * node) {
  float sum = 0, a, cn_1, cn_2, tmp;
  cn_2 = computeCN(node, node->get_eval()-500, -1);
  for (float x=-450; x<=500; x+=50) {
    cn_1 = cn_2;
    if (x==0) {
      cn_2 = 1;
    } else {
      cn_2 = computeCN(node, node->get_eval()+x, (x<0) ? -1 : 1);
    }
    a = (cn_2-cn_1)/50;
    tmp = (a != 0) ? (log(cn_2) - log(cn_1))/a : (50/cn_1);
    sum += tmp * (x<=0 ? -1 : 1);
  }
  return sum;
}

float CNPlayer::compute_cn_mean_eval(MinimaxNode * node) {
  float sum = 0, sump = 0, a, b, cn_1, cn_2, tmp;
  cn_2 = computeCN(node, node->get_eval()-500, -1);
  float eval = node->get_eval();
  for (float x=-450; x<=500; x+=50) {
    cn_1 = cn_2;
    if (x==0) {
      cn_2 = 1;
    } else {
      cn_2 = computeCN(node, node->get_eval()+x, (x<0) ? -1 : 1);
    }

    a = (cn_2-cn_1)/50;
    b = cn_2 - a*(eval+x);

    tmp = (a!=0) ? (log(cn_2) - log(cn_1))/a : (50/cn_1);
    sump += tmp;

    if (abs(a) >= EPS && abs(b) >= EPS) {
      tmp = 50.0/a + b*(log(cn_1)-log(cn_2))/sqr(a);
    } else{ 
      if (abs(a) < EPS) {
        tmp = (100*(eval+x)-2500)/(2*b);
      } else {
        tmp = 50/a;
      }
    }
//    cout << "[" << eval+x-50 << "," << cn_1 << "] [" << eval+x << "," << cn_2 << "] -> " << tmp << endl;
    sum += tmp;
  }
  return sum/sump;
}

float CNPlayer::negamax(MinimaxNode* node, OthelloGame* game, int depth) {
  node_visited ++;
  if (depth == 0 or game->isEnd()) {
    node->set_eval(eval_function(game, game->turn));
    node_terminal ++;
    delete game;
    return node->get_eval();
  }

  auto children = node->get_children(game);
  float best = -INF;
  float temp;
  OthelloGame * next_game;
  for (auto & child : (*children)) {
    next_game = new OthelloGame(game, child.first);
    child.second = MinimaxNode::create(next_game);
    temp = -negamax(child.second, next_game, depth-1);
    if (temp>best) {
      best = temp;
    }
  }
  node->set_eval(best);
  delete game;
  return best;
}

float CNPlayer::computeCN(MinimaxNode* node, float target, int sign) {
  if ((node->get_eval() - target)*sign > 0) {
    return 0;
  }
  if (node->is_terminal) {
    return INF;
  }

  auto children = node->get_children();
  // will need to change for hash
  if (children == NULL || children->size() == 0) {
    return 1;
  }

  float res = (sign == 1) ? INF : 0;
  float tmp;
  for (auto & child : (*children)) {
    tmp = computeCN(child.second, -target, -sign);
    res = (sign == 1) ? min(res, tmp) : (res + tmp);
  }

  return res;
}