#include <iostream>
#include <utility>
#include <list>
#include <cstdio>
#include "minimax_node.h"
#include "global_limits.h"
#include "othello_game.h"
#include "helper.h"

using namespace std;

int MinimaxNode::node_inst_created = 0;
int MinimaxNode::node_inst_destroyed = 0;
int MinimaxNode::node_inst_reused = 0;

typedef pair<int,int> hash_pair_t;

unordered_map<hash_pair_t, MinimaxNode*> MinimaxNode::cache(3000000);

MinimaxNode* MinimaxNode::create(OthelloGame* game) {
  /*
  auto found = cache.find(game->get_hash_key());
  if (true || found == cache.end()) {
    MinimaxNode* new_node = new MinimaxNode(game);
    cache.insert(make_pair(new_node->hash_key, new_node));
    return new_node;
  } else {
    node_inst_reused ++;
    return found->second;
  }
  */
  return new MinimaxNode(game);
}

// constructor
MinimaxNode::MinimaxNode(OthelloGame* game) {
  MinimaxNode::node_inst_created ++;
  this->children = NULL;
  this->search_depth = 0;

  this->turn = game->turn;
  this->current_turn = game->discs[1] + game->discs[2];
  this->is_terminal = game->isEnd();
  this->hash_key = game->get_hash_key();

  this->eval = -INF;
}

// destructor
MinimaxNode::~MinimaxNode() {
  MinimaxNode::node_inst_destroyed ++;
  if (this->children != NULL) {
    for (auto & child : (*this->children))
      delete child.second;
    delete this->children;
  }
}

void MinimaxNode::clear_cache(int tturn) {
  int count = 0;
  MinimaxNode* p;
  for (auto it=cache.begin(); it!=cache.end();) {
    if (it->second->current_turn <= tturn) {
      p = it->second;
      it=cache.erase(it);
      delete p;
      count ++;
    } else it++;
  }
  cout << "detele " << count << " nodes\n";
}

move_pair_t MinimaxNode::getBestMove() {
  float best_child_eval = INF;
  count_best_child = 0;
  for (auto & child : (*this->children)) {
    if (child.second->eval < best_child_eval) {
      best_child_eval = child.second->eval;
      best_move = child.first;
      count_best_child = 1;
    } else {
      if (child.second->eval == best_child_eval) {
        count_best_child ++;
      }
    }
  }
  return best_move;
}

vector<pair<move_pair_t, MinimaxNode*> >* MinimaxNode::get_children() {
  return children; // use with care
}

vector<pair<move_pair_t, MinimaxNode*> >* MinimaxNode::get_children(OthelloGame* game) {
  if (children == NULL) {
    generateChildren(game);
  }
  return children;
}

void MinimaxNode::remove_children() {
  if (children == NULL) return;
  for (auto & child : (*children)) {
    delete child.second;
  }
  delete children;
  children = NULL;
}

// private functions
// TO BE IMPROVED
void MinimaxNode::generateChildren(OthelloGame* game) {
  children = new vector<pair<move_pair_t, MinimaxNode*> >();
  // auto next_games = game->listNextGames(); // get list of pair of moves and corresponding games
  // for (auto & move_game_pair : (*next_games)) {
  //   children->push_back(make_pair(move_game_pair.first, MinimaxNode::create(move_game_pair.second)));
  //   delete move_game_pair.second;
  // }
  // delete next_games;
  auto move_list = game->list_next_moves();
  for (auto & move : (*move_list)) {
    children->push_back(mp(move, (MinimaxNode*)NULL));
  }
  delete move_list;
}

/*
int main() {
  OthelloGame o;
  o.printBoard();
  cout << "possible moves:\n";
  cout << "  ";
  for (auto & move : o.possible_moves) {
    cout << move.first << '-' << move.second << ',';
  }
  cout << endl;
  printf("winner is %d, player is %d\n", o.getWinner(), o.turn);
  printf("valid moves for first player:\n");
  printf("  ");
  for (auto & move : o.possible_moves) {
    if (o.isValidMove(move)) {
      printf("%d-%d, ", move.first, move.second);
    }
  }

  printf("\n");

  MinimaxNode *node = MinimaxNode::create(&o, simple_eval);
  printf("eval: %f\n", node->get_eval());

  //  ==============5 3================
  printf("play 5-3\n");

  o.progress(mp(5,3));
  o.printBoard();
  cout << "possible moves:\n";
  cout << "  ";
  for (auto & move : o.possible_moves) {
    cout << move.first << '-' << move.second << ',';
  }
  cout << endl;
  printf("player is %d\n", o.turn);
  printf("valid moves for second player:\n");
  printf("  ");
  for (auto & move : o.possible_moves) {
    if (o.isValidMove(move)) {
      printf("%d-%d, ", move.first, move.second);
    }
  }
  printf("\n");
  o.printResult();

  node = MinimaxNode::create(&o, simple_eval);
  printf("eval: %f\n", node->get_eval());

  //  ==============5 2=================
  printf("play 5-2\n");

  o.progress(mp(5,2));
  o.printBoard();
  cout << "possible moves:\n";
  cout << "  ";
  for (auto & move : o.possible_moves) {
    cout << move.first << '-' << move.second << ',';
  }
  cout << endl;
  printf("player is %d\n", o.turn);
  printf("valid moves for second player:\n");
  printf("  ");
  for (auto & move : o.possible_moves) {
    if (o.isValidMove(move)) {
      printf("%d-%d, ", move.first, move.second);
    }
  }
  printf("\n");
  o.printResult();

  node = MinimaxNode::create(&o, simple_eval);
  printf("eval: %f\n", node->get_eval());
}
*/