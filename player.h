#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "othello_game.h"
#include "helper.h"

class Player {
public:
  virtual move_pair_t selectMove(OthelloGame *game) { return make_pair(-1,-1); }
  virtual string get_name() { return "abstract player"; }
};

#endif