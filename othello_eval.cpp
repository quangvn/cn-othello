#include "othello_eval.h"
#include <fstream>

float OthelloEval::coef[6];

void OthelloEval::load_parameter() {
  ifstream in_file(CONFIG_FILE);
  for (int i=0; i<6; i++) {
    in_file >> coef[i];
  }
}

float OthelloEval::simple_eval(OthelloGame *game, int role) {
  int opp = 3 - role;
  float discs = game->discs[role] - game->discs[opp];
  float mobility = game->countValidMoves(role) - game->countValidMoves(opp);
  float corner = game->countCorners(role) - game->countCorners(opp);

  return 1000*corner + 100*mobility + 1*discs;
}

float OthelloEval::dynamic_heuristic_evaluation_function(OthelloGame *game, int role)  {
  int my_tiles = 0, opp_tiles = 0, i, j, k, my_front_tiles = 0, opp_front_tiles = 0, x, y;
  int my_color = role, opp_color = 3-role;

  char (*grid)[8] = (char(*)[8])malloc(sizeof(char[8])*8);
  for (int i=0; i<8; i++) {
    for (int j=0; j<8; j++) {
      grid[i][j] = game->board->get(i,j);
    }
  }

  double p = 0, c = 0, l = 0, m = 0, f = 0, d = 0;

  int X1[] = {-1, -1, 0, 1, 1, 1, 0, -1};
  int Y1[] = {0, 1, 1, 1, 0, -1, -1, -1};
  int V[8][8] = {
    {20, -3, 11, 8, 8, 11, -3, 20},
    {-3, -7, -4, 1, 1, -4, -7, -3},
    {11, -4, 2, 2, 2, 2, -4, 11},
    {8, 1, 2, -3, -3, 2, 1, 8},
    {8, 1, 2, -3, -3, 2, 1, 8},
    {11, -4, 2, 2, 2, 2, -4, 11},
    {-3, -7, -4, 1, 1, -4, -7, -3},
    {20, -3, 11, 8, 8, 11, -3, 20}
  };

// Piece difference, frontier disks and disk squares
  for(i=0; i<8; i++)
    for(j=0; j<8; j++)  {
      if(grid[i][j] == my_color)  {
        d += V[i][j];
        my_tiles++;
      } else if(grid[i][j] == opp_color)  {
        d -= V[i][j];
        opp_tiles++;
      }
      if(grid[i][j] != BLANK)   {
        for(k=0; k<8; k++)  {
          x = i + X1[k]; y = j + Y1[k];
          if(x >= 0 && x < OTHELLO_BOARD_SIZE && y >= 0 && y < OTHELLO_BOARD_SIZE && grid[x][y] == BLANK) {
            if(grid[i][j] == my_color)  my_front_tiles++;
            else opp_front_tiles++;
            break;
          }
        }
      }
    }    
  p = (my_tiles - opp_tiles);
  
  f = (my_front_tiles - opp_front_tiles);

// Corner occupancy
  c = game->countCorners(my_color) - game->countCorners(opp_color);    

// Corner closeness
  my_tiles = opp_tiles = 0;
  if(grid[0][0] == BLANK)   {
    if(grid[0][1] == my_color) my_tiles++;
    else if(grid[0][1] == opp_color) opp_tiles++;
    if(grid[1][1] == my_color) my_tiles++;
    else if(grid[1][1] == opp_color) opp_tiles++;
    if(grid[1][0] == my_color) my_tiles++;
    else if(grid[1][0] == opp_color) opp_tiles++;
  }
  if(grid[0][7] == BLANK)   {
    if(grid[0][6] == my_color) my_tiles++;
    else if(grid[0][6] == opp_color) opp_tiles++;
    if(grid[1][6] == my_color) my_tiles++;
    else if(grid[1][6] == opp_color) opp_tiles++;
    if(grid[1][7] == my_color) my_tiles++;
    else if(grid[1][7] == opp_color) opp_tiles++;
  }
  if(grid[7][0] == BLANK)   {
    if(grid[7][1] == my_color) my_tiles++;
    else if(grid[7][1] == opp_color) opp_tiles++;
    if(grid[6][1] == my_color) my_tiles++;
    else if(grid[6][1] == opp_color) opp_tiles++;
    if(grid[6][0] == my_color) my_tiles++;
    else if(grid[6][0] == opp_color) opp_tiles++;
  }
  if(grid[7][7] == BLANK)   {
    if(grid[6][7] == my_color) my_tiles++;
    else if(grid[6][7] == opp_color) opp_tiles++;
    if(grid[6][6] == my_color) my_tiles++;
    else if(grid[6][6] == opp_color) opp_tiles++;
    if(grid[7][6] == my_color) my_tiles++;
    else if(grid[7][6] == opp_color) opp_tiles++;
  }
  l = (my_tiles - opp_tiles);

// Mobility
  m = game->countValidMoves(my_color) - game->countValidMoves(opp_color);

  delete grid;

// final weighted score
  double score = (coef[0] * p) + (coef[1] * c) + (coef[2] * l) + (coef[3] * m) + (coef[4] * f) + (coef[5] * d);
  return score;
}