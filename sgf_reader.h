#ifndef __SGF_READER_H__
#define __SGF_READER_H__

#include <fstream>
#include <vector>
#include "othello_game.h"

class SgfReader {
public:
  void open(string filename);
  pair<int,move_pair_t> get_next_move();
  void close();
  bool is_open();
  bool is_end();
  vector<string> get_filenames_in_folder(string folder);


private:
  std::ifstream file;
  bool open_flag, end_flag;
};

#endif