#include <iostream>
#include "match_manager.h"
#include "player.h"
#include "human_player.h"
#include "negamax_player.h"
#include "alpha_beta_player.h"
#include "othello_game.h"
#include "minimax_node.h"
#include "sgf_reader.h"
#include "cn_player.h"
#include "combined_player.h"
#include "helper.h"
#include <ctime>
#include <vector>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

void MatchManager::newMatch(int turn) {
  Player *player_1, *player_2;

  // human player plays first
  if (turn == 1) { 
    player_1 = new HumanPlayer();
    player_2 = new NegamaxPlayer();
  } else {
    player_1 = new NegamaxPlayer();
    player_2 = new HumanPlayer();
  }

  OthelloGame* game = new OthelloGame();
  // init game
  game->printBoard();
  while (not game->isEnd()){
    move_pair_t move;
    if (game->isFirstPlayerTurn()) {
      cout << "First player turn\n";
    } else {
      cout << "Second player turn\n";
    }
    if (game->isFirstPlayerTurn()) {
      move = player_1->selectMove(game);
    } else {
      move = player_2->selectMove(game);
    }
    cout << "  play: " << move.first << "-" << move.second << endl;
    game->progress(move);
    game->printBoard();
  }
  game->printResult();
  delete game;
  return;
}

void MatchManager::newComputerMatch() {
  int depth;
  cout << "Choose depth: ";
  cin >> depth;
  time_t timer = time(NULL);

  Player *player_1, *player_2;

  // player_1 = new AlphaBetaPlayer(depth, OthelloEval::simple_eval);
  // player_2 = new AlphaBetaPlayer(depth, OthelloEval::simple_eval);
  player_1 = new AlphaBetaPlayer(depth, OthelloEval::dynamic_heuristic_evaluation_function);
  player_2 = new AlphaBetaPlayer(depth, OthelloEval::dynamic_heuristic_evaluation_function);

  // player_1 = new NegamaxPlayer(depth, OthelloEval::dynamic_heuristic_evaluation_function);
  // player_2 = new NegamaxPlayer(depth, OthelloEval::dynamic_heuristic_evaluation_function);

  // player_1 = new NegamaxPlayer(depth, OthelloEval::simple_eval);
  // player_2 = new NegamaxPlayer(depth, OthelloEval::simple_eval);

  OthelloGame* game = new OthelloGame();
  // init game
  game->printBoard();
  while (not game->isEnd()){
    move_pair_t move;
    if (game->isFirstPlayerTurn()) {
      cout << "First player turn\n";
    } else {
      cout << "Second player turn\n";
    }
    if (game->isFirstPlayerTurn()) {
      move = player_1->selectMove(game);
    } else {
      move = player_2->selectMove(game);
    }
    cout << "  play: " << move.first << "-" << move.second << endl;
    game->progress(move);
    game->printBoard();
    // MinimaxNode::clear_cache(game->discs[1] + game->discs[2]);
  }
  game->printResult();
  delete game;

  cout << "Take " << time(NULL) - timer << " seconds!\n";
  return;
}

void MatchManager::compareComputer() {
  vector<Player *> players;
  // players.push_back(new AlphaBetaPlayer(3, OthelloEval::dynamic_heuristic_evaluation_function));
  // players.push_back(new AlphaBetaPlayer(3, OthelloEval::dynamic_heuristic_evaluation_function));
  players.push_back(new AlphaBetaPlayer(5, OthelloEval::dynamic_heuristic_evaluation_function));
  players.push_back(new CombinedPlayer(2, 2, OthelloEval::dynamic_heuristic_evaluation_function));
  players.push_back(new CombinedPlayer(4, 0, OthelloEval::dynamic_heuristic_evaluation_function));

  // players.push_back(new AlphaBetaPlayer(7, OthelloEval::dynamic_heuristic_evaluation_function));
  // players.push_back(new CNPlayer(3, OthelloEval::dynamic_heuristic_evaluation_function));
  // players.push_back(new CNPlayer(5, OthelloEval::dynamic_heuristic_evaluation_function));

  int match_count = 20;
  ofstream log_file("random_pos_test.txt", ios::out | ios::app);
  for (int i=0; i<players.size()-1; i++) {
    for (int j=i+1; j<players.size(); j++) {
      float score = 0;
      srand(1); // reset random seed to get the same set of matches
      for (int k=0; k<match_count; k++) {
        score += random_pos_match(players[i], players[j]);
      }
      log_file << players[i]->get_name() << " " << score << " | " << 2.0*match_count-score << " " << players[j]->get_name() << endl;
    }
  }
  return;
}

// take 2 players, choose randomly a starting position 
// play 2 games, each player will make the first move once
// return the score for first player
// a win is 1 point
// a draw is 1/2 point
// a lost is 0
float MatchManager::random_pos_match(Player * p1, Player * p2) {
  // game transcript folder is fixed as 'test'
  string folder = "test";
  SgfReader r;
  vector<string> filenames = r.get_filenames_in_folder(folder);

  // select a random match
  string file = folder + "\\" + filenames[rand() % filenames.size()];
  // select a random starting position between 5 and 10 moves
  int start_pos = rand() % 6 + 5;
  cout << "file: " << file << ", position: " << start_pos << endl;
  // open game file
    r.open(file);

  vector<pair<int, move_pair_t>> move_list;
  if (! r.is_open()) {
    cout << "Cannot open file\n";
    return 1;
  }

  while (! r.is_end()) {
    move_list.push_back(r.get_next_move());
  }

  // first round
  pair<int, move_pair_t> move;
  OthelloGame * game = new OthelloGame();
  for (int i=0; i<start_pos-1; i++) {
    move = move_list[i];
    if (game->turn != move.first) {
      game->progress(make_pair(-1,-1));
    }
    game->progress(move.second);
  }
  game->printBoard();

  while (not game->isEnd()) {
    move_pair_t move;
    if (game->isFirstPlayerTurn()) {
      // cout << p1->get_name() << endl;
      move = p1->selectMove(game);
    } else {
      // cout << p2->get_name() << endl;
      move = p2->selectMove(game);
    }
    game->progress(move);
    // game->printBoard();
  }
  game->printBoard();
  game->printResult();
  float score = game->get_first_player_score();

  // second round
  game = new OthelloGame();
  for (int i=0; i<start_pos-1; i++) {
    move = move_list[i];
    if (game->turn != move.first) {
      game->progress(make_pair(-1,-1));
    }
    game->progress(move.second);
  }
  game->printBoard();

  while (not game->isEnd()) {
    move_pair_t move;
    if (game->isFirstPlayerTurn()) {
      // cout << p2->get_name() << endl;
      move = p2->selectMove(game);
    } else {
      // cout << p1->get_name() << endl;
      move = p1->selectMove(game);
    }
    game->progress(move);
    // game->printBoard();
  }
  game->printBoard();
  game->printResult();

  return score + 1 - game->get_first_player_score();
}

void MatchManager::analyze_game() {
  cout << "Game file: ";
  string filename;
  cin >> filename;

  SgfReader r;
  r.open(filename);

  vector<pair<int,move_pair_t>> move_list;
  if (! r.is_open()) {
    cout << "Cannot open file\n";
    return;
  }

  while (! r.is_end()) {
    move_list.push_back(r.get_next_move());
  }

  NegamaxPlayer* player_e2 = new NegamaxPlayer(9, OthelloEval::dynamic_heuristic_evaluation_function);
  player_e2->LOG_FILE = "log\\ab_9.txt";
  NegamaxPlayer* player_e1 = new NegamaxPlayer(4, OthelloEval::simple_eval);
  player_e1->LOG_FILE = "log\\composed_cn_4_1.txt";

  OthelloGame* game = new OthelloGame();
  for (auto move : move_list) {
    game->printBoard();
    // analyze
    if (game->turn == 1) {
      //player_e1->selectMove(game);
      //player_e2->selectMove(game);
      player_e2->rank_move_by_alpha_beta(game);
    }

    // progress
    if (game->turn != move.first) {
      cout << "player " << game->turn << " passed.\n";
      game->progress(make_pair(-1,-1));
    }
    cout << move.second.first << "-" << move.second.second << endl;
    game->progress(move.second);
  }
  game->printBoard();
}

void MatchManager::analyze_folder() {
  cout << "Folder: ";
  string folder;
  cin >> folder;

  SgfReader r;
  vector<string> filenames = r.get_filenames_in_folder(folder);

  string output1, output2, output3;
  int c = 0;
  for(string file : filenames) {
    c++;
    output1 = output2 = output3 = file;
    output1 = "log\\" + output1.insert(output1.find("."),"_cn");
    output1.replace(output1.find(".")+1,3,"txt");
    output2 = "log\\" + output2.insert(output2.find("."),"_ab");
    output2.replace(output1.find(".")+1,3,"txt");
    output3 = "log\\" + output3.insert(output3.find("."),"_node_count");
    output3.replace(output1.find(".")+1,3,"txt");
    cout << "processing: " + file + "\n";
    cout << "  output: " << output1 << ", " << output2 << endl;

    analyze_cn_game(folder + "\\" + file, output1, output2, output3);
  }
  cout << "\nProcessed " << c << " files\n";
}

void MatchManager::analyze_cn_game(string filename, string out1, string out2, string out3) {
  SgfReader r;
  r.open(filename);

  vector<pair<int,move_pair_t>> move_list;
  if (! r.is_open()) {
    cout << "Cannot open file\n";
    return;
  }

  while (! r.is_end()) {
    move_list.push_back(r.get_next_move());
  }

  cout << "..got moves' list\n";

  NegamaxPlayer* cn_player = new NegamaxPlayer(5, OthelloEval::dynamic_heuristic_evaluation_function);
  cn_player->LOG_FILE = out1;
  NegamaxPlayer* ab_player_5 = new NegamaxPlayer(5, OthelloEval::dynamic_heuristic_evaluation_function);
  ab_player_5->LOG_FILE = out2;
  NegamaxPlayer* ab_player_3 = new NegamaxPlayer(3, OthelloEval::dynamic_heuristic_evaluation_function);
  ab_player_3->LOG_FILE = out2;
  NegamaxPlayer* ab_player_7 = new NegamaxPlayer(7, OthelloEval::dynamic_heuristic_evaluation_function);
  out2.insert(out2.find("."),"_7");
  ab_player_7->LOG_FILE = out2;

  OthelloGame* game = new OthelloGame();
  for (int i=0; i<move_list.size(); i++) {
    cout << i << ".."; 
    auto move = move_list[i];
    game->printBoard();
    // analyze
    if (i < 55 && game->turn == 1) {
      // ofstream log_file(out3, ios::out | ios::app);
      // log_file << cn_player->rank_move(game) << "," << ab_player_5->rank_move_by_alpha_beta(game) << endl;
      cn_player->rank_move(game);
      // ab_player_5->rank_move_by_alpha_beta(game);
      // ab_player_3->rank_move_by_alpha_beta(game);
      // log_file.close();
      // ab_player_7->rank_move_by_alpha_beta(game);
    }

    // progress
    if (game->turn != move.first) {
      cout << "player " << game->turn << " passed.\n";
      game->progress(make_pair(-1,-1));
    }
    cout << move.second.first << "-" << move.second.second << endl;
    game->progress(move.second);
  }
  game->printBoard();
}