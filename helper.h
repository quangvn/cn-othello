#ifndef __HELPER_H__
#define __HELPER_H__

#include <sstream>

#define forf(x,c) for(int x=0; x<c; x++)

#define mp(x,y) make_pair(x, y)

// TEMPORARY FIX
template <typename T>
string to_string(T value)
{
  ostringstream os ;
  os << value ;
  return os.str() ;
}
//

#endif