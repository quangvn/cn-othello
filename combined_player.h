#ifndef __COMBINED_PLAYER__
#define __COMBINED_PLAYER__

#include "player.h"
#include "othello_game.h"
#include "minimax_node.h"
#include "othello_eval.h"

class CombinedPlayer : public Player {
public:
  static int node_visited;
  static int node_terminal;
  string LOG_FILE;

  CombinedPlayer();
  CombinedPlayer(int cd, int ad, othello_eval e);
  move_pair_t selectMove(OthelloGame *game);

  string get_name() {
    return "combined player(" + to_string(cn_depth) + "," + to_string(ab_depth) + ")";
  }

private:
  float negamax(MinimaxNode* node, OthelloGame* game, int depth);
  float alphabeta(MinimaxNode* node, OthelloGame* game, float alpha, float beta, int depth);
  float computeCN(MinimaxNode* node, float target, int sign);
  float compute_cn_adjustment(MinimaxNode* node);
  float compute_cn_mean_eval(MinimaxNode * node);

  int cn_depth, ab_depth;
  int search_depth;
  othello_eval eval_function;
};

#endif