#ifndef _OTHELLO_GAME_H__
#define _OTHELLO_GAME_H__

#include <list>
#include <utility>
#include <cstring>
#include <iostream>
#include <unordered_set>
#include <cstdlib>

#ifndef BLANK
#define BLANK 0
#endif

#ifndef FIRST_PLAYER
#define FIRST_PLAYER 1
#endif

#ifndef SECOND_PLAYER
#define SECOND_PLAYER 2
#endif

using namespace std;

typedef pair<int, int> move_pair_t;

namespace std {
  template <>
    struct hash<move_pair_t> {
      size_t operator()(move_pair_t const & x) const noexcept {
        return (51 + hash<int>()(x.first)) * 51 + hash<int>()(x.second);
      }
    };
}

#define OTHELLO_BOARD_SIZE 8

class OthelloHash {
public:
  long long HASH_1[2][OTHELLO_BOARD_SIZE*OTHELLO_BOARD_SIZE];
  long long HASH_2[2][OTHELLO_BOARD_SIZE*OTHELLO_BOARD_SIZE];

  OthelloHash();
};

class OthelloBoard {
public:
  OthelloBoard() {
    this->init();
  }
  OthelloBoard(OthelloBoard* a_board) {
    memcpy(b1, a_board->b1, sizeof(b1));
    memcpy(b2, a_board->b2, sizeof(b2));
  }
  int get(int x, int y) {
    unsigned int t = 1 << y;
    if ((b1[x] & t) != 0) return 1;
    if ((b2[x] & t) != 0) return 2;
    return 0;
  }
  void set(int x, int y, int val) {
    unsigned int t = 1 << y;
    b1[x] = b1[x] & (~t);
    b2[x] = b2[x] & (~t);
    if (val == 1) {
      b1[x] |= t;
    } else {
      b2[x] |= t;
    }
  }
  void init() {
    memset(b1, 0, sizeof(b1));
    memset(b2, 0, sizeof(b2));
    set(3,3,2);
    set(3,4,1);
    set(4,3,1);
    set(4,4,2);
  }
  bool isValid(const move_pair_t & move) {
    return (move.first>=0) and (move.first<OTHELLO_BOARD_SIZE) and (move.second>=0) and (move.second<OTHELLO_BOARD_SIZE);
  }
  bool isValid(int x, int y) {
    return (x>=0) and (x<OTHELLO_BOARD_SIZE) and (y>=0) and (y<OTHELLO_BOARD_SIZE);
  }

  unsigned int b1[OTHELLO_BOARD_SIZE], b2[OTHELLO_BOARD_SIZE];
};

class OthelloGame {
public:
  static int game_inst_created;
  static int game_inst_destroyed;
  static OthelloHash othello_hash;
  OthelloGame();
  OthelloGame(OthelloGame &game);
  OthelloGame(OthelloGame *game);
  OthelloGame(OthelloGame *game, move_pair_t move);

  // clone
  OthelloGame* clone();

  // destructors
  ~OthelloGame();

  bool isEnd();

  bool isValidMove(int x, int y);
  bool isValidMove(move_pair_t move);
  bool isValidMove(int x, int y, int role);

  void init(); // starting position
  void progress(move_pair_t move);
  void progress(string move);
  OthelloGame* getNextGame(move_pair_t move);
  list<pair<move_pair_t, OthelloGame*> >* listNextGames();

  list<move_pair_t>* list_next_moves();

  // update
  void updateStatus(move_pair_t move);
  // void updatePossibleMoves(move_pair_t move);
  // void checkPossibleEnd(move_pair_t move);

  // assume that isEnd() is already check
  int getWinner();
  float get_first_player_score() {
    if (discs[1] > discs[2]) {
      return 1;
    }
    if (discs[1] == discs[2]) {
      return 0.5;
    }
    return 0;
  }
  inline int nextTurn() { return 3-turn; }
  move_pair_t convertMove(string move);
  pair<int,int> get_hash_key() { return hash_key; }

  void printResult();
  void printBoard();
  bool isFirstPlayerTurn() {return turn == 1;}

  inline int convertCoord(int x, int y) {
    return x*OTHELLO_BOARD_SIZE + y;
  }

  int countValidMoves(int role);
  int countCorners(int role);

  unordered_set<move_pair_t> possible_moves;
  // -1: first player win
  // -2: second player win
  // -3: draw
  // 1: first player
  // 2: second player
  int turn;
  pair<int,int> hash_key;
  int discs[3];
  OthelloBoard* board;
  int winner;
};

#endif