#include <iostream>
#include "human_player.h"

using namespace std;

move_pair_t HumanPlayer::selectMove(OthelloGame *game) {
  int x, y;
  while (true) {
    cout << "Please choose a move: ";
    cin >> x >> y;
    move_pair_t move = make_pair(x,y);
    if (game->isValidMove(move)) {
      return move;
    }
    cout << "Invalid move." << endl;
  }
}