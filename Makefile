CXX=g++
CXXFLAGS=-g -std=c++11 -pedantic

exec = game
src=$(filter-out analyzer.cpp, $(wildcard *.cpp))
obj=$(src:%.cpp=%.o)

$(exec) : $(obj)
	$(CXX) $(CXXFLAGS) $(obj) -o $(exec).exe

analyzer: analyzer.cpp
	$(CXX) $(CXXFLAGS) -o analyzer analyzer.cpp

othello_eval.o: othello_eval.cpp othello_eval.h othello_game.h helper.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

match_manager.o: match_manager.cpp match_manager.h othello_eval.h helper.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

negamax_player.o: negamax_player.cpp negamax_player.h othello_eval.h helper.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

alpha_beta_player.o: alpha_beta_player.cpp alpha_beta_player.h othello_eval.h helper.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

combined_player.o: combined_player.cpp combined_player.h othello_eval.h helper.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

cn_player.o: cn_player.cpp cn_player.h othello_eval.h helper.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.cpp %.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -f *.o
	rm -f *.exe