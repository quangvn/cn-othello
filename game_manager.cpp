#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "match_manager.h"
#include "othello_eval.h"

using namespace std;

void PrintEmptyLine(int length) {
  char *a = new char[length];
  for (int i=0; i<length; i++) {
    *(a+i) = '-';
  }
  *(a+length) = '\0';
  cout << a << endl;
}

void init() {
  OthelloEval o;
  o.load_parameter();
}

void ShowPlayMenu() {
  MatchManager match_manager;
  while (true) {
    cout << "Choose game type" << endl;
    PrintEmptyLine(20);
    cout << "1: play with computer as first player" << endl;
    cout << "2: play with computer as second player" << endl;
    cout << "3: watching computer play" << endl;
    cout << "4: compare computers" << endl;
    cout << "5: analyze game" << endl;
    cout << "6: analyze all game in folder" << endl;
    cout << "9: clear" << endl;
    cout << "0: exit" << endl;
    PrintEmptyLine(20);
    int command;
    cin >> command;
    switch (command) {
      case 1:
        match_manager.newMatch(1);
        break;
      case 2:
        match_manager.newMatch(2);
        break;
      case 3:
        match_manager.newComputerMatch();
        break;
      case 4:
        match_manager.compareComputer();
        break;
      case 5:
        match_manager.analyze_game();
        break;
      case 6:
        match_manager.analyze_folder();
        break;
      case 8:
        init();
        break;
      case 9:
        system("printf \"\\033c\"");
        break;
      case 0:
        return;
        break;
      default:
        break;
        // do nothing
    }
  }
}

void ShowMenu() {
  ShowPlayMenu();
/*
  while (true) {
    system("printf \"\\033c\"");
    cout << "" << endl;
    PrintEmptyLine(20);
    cout << "MENU" << endl;
    cout << "-- \'play\' to play with computer" << endl;
    cout << "-- \'exit\' to quit" << endl;
    PrintEmptyLine(20);
    string command;
    cin >> command;
    if (command == "play") {
      ShowPlayMenu();
    }
    if (command == "exit") {
      exit(0);
    }
  }
*/
}

int main(){
  init();
  ShowMenu();  
}