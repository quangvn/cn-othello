#ifndef __NEGAMAX_PLAYER__
#define __NEGAMAX_PLAYER__

#include "player.h"
#include "othello_game.h"
#include "minimax_node.h"
#include "othello_eval.h"

class NegamaxPlayer : public Player {
public:
  static int node_visited;
  static int node_terminal;
  string LOG_FILE;

  NegamaxPlayer();
  NegamaxPlayer(int depth, othello_eval e);
  move_pair_t selectMove(OthelloGame *game);
  int rank_move(OthelloGame *game);
  int rank_move_by_alpha_beta(OthelloGame *game);

  string get_name() {
    return "negamax player(" + to_string(search_depth) + ")";
  }

private:
  float alphabeta(MinimaxNode* node, OthelloGame* game, float alpha, float beta, int depth);
  float negamax(MinimaxNode* node, OthelloGame* game, int depth);
  float negamax_no_remember(MinimaxNode* node, OthelloGame *game, int depth);
  float computeCN(MinimaxNode* node, float target, int sign);
  float compute_cn_adjustment(MinimaxNode* node);
  float compute_cn_mean_eval(MinimaxNode * node);
  
  int search_depth;
  othello_eval eval_function;
};

#endif