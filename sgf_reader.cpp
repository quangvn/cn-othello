#include "sgf_reader.h"
#include <iostream>
#include <fstream>
#include <Windows.h>

void SgfReader::open(string filename) {
  this->file.open(filename, std::ifstream::in);
  if (! this->file.is_open()) return;
  // skip header
  char c;
  cout << "start read file\n";
  this->file.get(c); // '('
  this->file.get(c); // ';'
  while (this->file.get(c)) {
//  cout << c;
    if (c == ';') break;
  }
  cout << "opened\n";
  this->open_flag = true;
  this->end_flag = false;
}

pair<int,move_pair_t> SgfReader::get_next_move() {
  char c3,c1,c2;
  this->file.get(c3); // B or W
  this->file.get(); // [
  this->file.get(c1); // 'a'-'h'
  this->file.get(c2); // '1'-'8'

  char c;
  while (this->file.get(c)) {
    if (c == ';') break;
    if (c == ')') {
      this->end_flag = true;
      break;
    }
  }
  return make_pair(c3 == 'B' ? 1 : 2,make_pair(c1-'a', c2-'1'));
}

bool SgfReader::is_open() {
  return this->open_flag;
}

bool SgfReader::is_end() {
  return this->end_flag;
}

void SgfReader::close() {
  this->file.close();
  this->open_flag = false;
}

vector<string> SgfReader::get_filenames_in_folder(string folder)
{
    vector<string> names;
    char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) { 
        do { 
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {
                names.push_back(fd.cFileName);
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    } 
    return names;
}