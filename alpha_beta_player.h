#ifndef __ALPHA_BETA_PLAYER_H__
#define __ALPHA_BETA_PLAYER_H__

#include "player.h"
#include "othello_game.h"
#include "minimax_node.h"
#include "othello_eval.h"

class AlphaBetaPlayer : public Player {
public:
  static int node_visited;

  AlphaBetaPlayer();
  AlphaBetaPlayer(int depth, othello_eval e);
  move_pair_t selectMove(OthelloGame *game);
  string get_name() {
    return "alpha-beta player(" + to_string(search_depth) + ")";
  }

private:
  float alphabeta(MinimaxNode* node, OthelloGame * game, float alpha, float beta, int depth);
  
  int search_depth;
  bool logging;
  othello_eval eval_function;
};

#endif