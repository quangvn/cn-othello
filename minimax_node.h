#ifndef __MINIMAX_NODE_H__
#define __MINIMAX_NODE_H__

#include <list>
#include <utility>
#include <unordered_map>
#include <vector>
#include "othello_game.h"

using namespace std;

class MinimaxNode{
public:
  // Factory
  static MinimaxNode* create(OthelloGame* game);
  static int node_inst_created;
  static int node_inst_destroyed;
  static int node_inst_reused;
  static unordered_map<pair<int,int>, MinimaxNode*> cache; // pair of hash keys

  ~MinimaxNode();

  static void clear_cache(int tturn);


  // MinimaxNode* getChild(move_pair_t move); // not need
  move_pair_t getBestMove();

  bool isTerminal(OthelloGame* game) { return game->isEnd(); }

// setters and getters
  float get_eval() { return eval; }
  void set_eval(float eval_) { eval = eval_; }

  int get_search_depth() { return search_depth; }
  void set_search_depth(int search_depth_) { search_depth = search_depth_; }

  vector<pair<move_pair_t, MinimaxNode*> >* get_children();
  vector<pair<move_pair_t, MinimaxNode*> >* get_children(OthelloGame* game);
  void remove_children();

  int count_best_child;
  pair<int,int> hash_key;
  bool is_terminal;

private:
  MinimaxNode(OthelloGame* game);
  void generateChildren(OthelloGame* game);

  float eval;
  int current_turn;
  int turn;
  int search_depth;
  move_pair_t best_move;
  vector<pair<move_pair_t, MinimaxNode*> >* children;
};

#endif