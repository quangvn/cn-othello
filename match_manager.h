#ifndef __MATCH_MANAGER_H__
#define __MATCH_MANAGER_H__

#include "player.h"

using namespace std;

class MatchManager {
public:
  void newMatch(int turn);
  void newComputerMatch();
  void compareComputer();
  void analyze_game();
  void analyze_folder();
  void analyze_cn_game(string, string, string, string);
  float random_pos_match(Player* p1, Player * p2);
};

#endif 