#include <vector>
#include <cstdio>
#include <sstream>
#include "othello_game.h"
#include "helper.h"

#define HASH_MOD_1 19890263
#define HASH_MOD_2 19890191

#define HASH_BASE_1 198929
#define HASH_BASE_2 890177

string theme[4] = {".", "●", "o", "-"};
// clockwise, first is 3h
const int tx[8] = { 0, 1, 1, 1, 0,-1,-1,-1};
const int ty[8] = { 1, 1, 0,-1,-1,-1, 0, 1};

int OthelloGame::game_inst_created = 0;
int OthelloGame::game_inst_destroyed = 0;
OthelloHash OthelloGame::othello_hash;

/*
  HASH_*[0]: for player 1
  HASH_*[1]: for player 2
  -> HASH_*[turn-1]
*/
OthelloHash::OthelloHash() {
  HASH_1[0][0] = (long long) HASH_BASE_1*HASH_BASE_1 % HASH_MOD_1;
  HASH_1[1][0] = (long long) HASH_BASE_2*HASH_BASE_2 % HASH_MOD_1;

  HASH_2[0][0] = (long long) HASH_BASE_1*HASH_BASE_1 % HASH_MOD_2;
  HASH_2[1][0] = (long long) HASH_BASE_2*HASH_BASE_2 % HASH_MOD_2;

  for (int i=1; i<OTHELLO_BOARD_SIZE*OTHELLO_BOARD_SIZE; i++) {
    HASH_1[0][i] = (long long) (HASH_1[0][i-1]*HASH_BASE_1 + HASH_BASE_2) % HASH_MOD_1;
    HASH_1[1][i] = (long long) (HASH_1[1][i-1]*HASH_BASE_2 + HASH_BASE_1) % HASH_MOD_1;

    HASH_2[0][i] = (long long) (HASH_2[0][i-1]*HASH_BASE_1 + HASH_BASE_2) % HASH_MOD_2;
    HASH_2[1][i] = (long long) (HASH_2[1][i-1]*HASH_BASE_2 + HASH_BASE_1) % HASH_MOD_2;
  }
}

OthelloGame::OthelloGame() {
  game_inst_created ++;
  this->init();
}

// copy constructors
OthelloGame::OthelloGame(OthelloGame &game) {
  game_inst_created ++;
  this->board = new OthelloBoard(game.board);
  this->turn = game.turn;
  //this->hash_key = game.hash_key;
  this->possible_moves = game.possible_moves;
  memcpy(this->discs, game.discs, sizeof(int) * 3); // do I really need this?
  this->winner = game.winner;
}

OthelloGame::OthelloGame(OthelloGame *game) {
  game_inst_created ++;
  this->turn = game->turn;
  //this->hash_key = game->hash_key;
  this->board = new OthelloBoard(game->board);
  this->possible_moves = game->possible_moves;
  memcpy(this->discs, game->discs, sizeof(int) * 3); // do I really need this?
  this->winner = game->winner;
}

// from game, play move
OthelloGame::OthelloGame(OthelloGame* game, move_pair_t move) {
  game_inst_created ++;
  this->turn = game->turn;
  //this->hash_key = game->hash_key;
  this->board = new OthelloBoard(game->board);
  this->possible_moves = game->possible_moves;
  memcpy(this->discs, game->discs, sizeof(int) * 3); // do I really need this?
  this->winner = game->winner;
  this->progress(move);
}

// clone
OthelloGame* OthelloGame::clone() {
  OthelloGame* new_game = new OthelloGame(this);
  return new_game;
}

// destructors
OthelloGame::~OthelloGame() {
  game_inst_destroyed ++;
  delete board;
}

void OthelloGame::init() {
  board = new OthelloBoard();
  turn = 1;
  winner = 0;
  discs[1] = discs[2] = 2;
  // hash_key = make_pair(0,0);
  // for (int i=3; i<5; ++i) {
  //   for (int j=3; j<5; ++j) {
  //     hash_key.first = (hash_key.first + othello_hash.HASH_1[board->get(i,j)-1][convertCoord(i,j)]) % HASH_MOD_1;
  //     hash_key.second = (hash_key.second + othello_hash.HASH_2[board->get(i,j)-1][convertCoord(i,j)]) % HASH_MOD_2;
  //   }
  // }
  move_pair_t pmove[] = {mp(2,2),mp(2,3),mp(2,4),mp(2,5),mp(3,5),mp(4,5),
    mp(5,5),mp(5,4),mp(5,3),mp(5,2),mp(4,2),mp(3,2)};
  for (auto & move : pmove) {
    possible_moves.insert(move);
  }
}

/*
  assume that position x,y is a blank cell 
*/
bool OthelloGame::isValidMove(int x, int y) {
  return isValidMove(x,y,this->turn);
}

bool OthelloGame::isValidMove(int x, int y, int role) {
  int opp = 3 - role;
  int nx, ny;
  bool can_jump;
  for (int i=0; i<8; i++) {
    nx = x+tx[i];
    ny = y+ty[i];
    can_jump = false;
    while (this->board->isValid(nx,ny) and this->board->get(nx,ny) == opp) {
      can_jump = true;
      nx += tx[i];
      ny += ty[i];
    }
    if (can_jump and this->board->isValid(nx,ny) and this->board->get(nx,ny) == role) {
      return true;
    }
  }
  return false;
}

bool OthelloGame::isValidMove(move_pair_t move) {
  return isValidMove(move.first, move.second, this->turn);
}

void OthelloGame::progress(move_pair_t move) {
  if (move.first == -1) {
    // if (turn == 1) {
    //   hash_key.first = (hash_key.first + 1) % HASH_MOD_1;
    //   hash_key.second = (hash_key.second + 1) % HASH_MOD_2;
    // } else {
    //   hash_key.first = (hash_key.first - 1) % HASH_MOD_1;
    //   hash_key.second = (hash_key.second - 1) % HASH_MOD_2;
    // }

    turn = nextTurn();
    return;
  }

  updateStatus(move);
}

void OthelloGame::progress(string move) {
  auto move_ = convertMove(move);
  this->progress(move_);
}

OthelloGame* OthelloGame::getNextGame(move_pair_t move) {
  OthelloGame* next_game = new OthelloGame(this, move);
  return next_game;
}

list<pair<move_pair_t, OthelloGame*> >* OthelloGame::listNextGames() {
  auto move_game_list = new list<pair<move_pair_t, OthelloGame*> >();
  for (auto & move : possible_moves) {
    if (isValidMove(move.first, move.second)) {
      move_game_list->push_back(make_pair(move, new OthelloGame(this, move)));
    }
  }
  if (move_game_list->size() == 0) {
    move_game_list->push_back(mp(mp(-1,-1), new OthelloGame(this, mp(-1,-1))));
  }
  return move_game_list;
}

list<move_pair_t>* OthelloGame::list_next_moves() {
  auto move_list = new list<move_pair_t>();
  for (auto & move : possible_moves) {
    if (isValidMove(move.first, move.second)) {
      move_list->push_back(move);
    }
  }
  if (move_list->size() == 0) {
    move_list->push_back(mp(-1,-1));
  }
  return move_list;
}

  // update
void OthelloGame::updateStatus(move_pair_t move) {
  board->set(move.first, move.second, turn);
  // hash_key.first = (hash_key.first + othello_hash.HASH_1[turn-1][convertCoord(move.first, move.second)]) % HASH_MOD_1;
  // hash_key.second = (hash_key.second + othello_hash.HASH_2[turn-1][convertCoord(move.first, move.second)]) % HASH_MOD_2;
  // if (turn == 1) {
  //   hash_key.first = (hash_key.first + 1) % HASH_MOD_1;
  //   hash_key.second = (hash_key.second + 1) % HASH_MOD_2;
  // } else {
  //   hash_key.first = (hash_key.first - 1) % HASH_MOD_1;
  //   hash_key.second = (hash_key.second - 1) % HASH_MOD_2;
  // }

  // update board state
  int nx, ny;
  int opp = 3 - this->turn;
  this->discs[turn] ++;
  vector<move_pair_t> cells;
  forf(i,8) {
    nx = move.first + tx[i];
    ny = move.second + ty[i];
    cells.clear();
    while (this->board->isValid(nx,ny) and this->board->get(nx,ny) == opp) {
      cells.push_back(mp(nx,ny));
      nx += tx[i];
      ny += ty[i];
    }
    if (this->board->isValid(nx,ny) and this->board->get(nx,ny) == this->turn) {
      // flip
      for (auto & cell : cells) {
        this->board->set(cell.first, cell.second, this->turn);
        // update hash
        // hash_key.first = (hash_key.first
        //   + HASH_MOD_1
        //   - othello_hash.HASH_1[2-turn][convertCoord(cell.first, cell.second)]
        //   + othello_hash.HASH_1[turn-1][convertCoord(cell.first, cell.second)]) % HASH_MOD_1;
        // hash_key.second = (hash_key.second
        //   + HASH_MOD_2
        //   - othello_hash.HASH_2[2-turn][convertCoord(cell.first, cell.second)]
        //   + othello_hash.HASH_2[turn-1][convertCoord(cell.first, cell.second)]) % HASH_MOD_2;
      }
      this->discs[this->turn] += cells.size();
      this->discs[opp] -= cells.size();
    }
  }

  // update possible moves
  this->possible_moves.erase(move);
  forf(i,8) {
    nx = move.first + tx[i];
    ny = move.second + ty[i];
    if (this->board->isValid(nx,ny) and this->board->get(nx,ny) == BLANK ) {
      this->possible_moves.insert(mp(nx,ny));
    }
  }

  turn = nextTurn();

  for (auto & move : possible_moves) {
    if (isValidMove(move.first, move.second, 1) || isValidMove(move.first, move.second, 2)) {
      return;
    }
  }

  // if cannot find any valid move for either players, game's end
  // update winner
  if (this->discs[1] > this->discs[2]) {
    winner = 1;
  } else if (this->discs[1] < this->discs[2]) {
    winner = 2;
  } else {
    winner = 3; // draw
  }
}

bool OthelloGame::isEnd() {
  return (this->winner == 0) ? false : true;
}

int OthelloGame::getWinner() {
  return this->winner;
}

void OthelloGame::printResult() {
  cout << discs[1] << "-" << discs[2] << endl;
  if (!isEnd()) return;
  if (getWinner() == 1) {
    cout << "First player win!\n";
  } else if (getWinner() == 2) {
    cout << "Second player win!\n";
  } else {
    cout << "Draw!!!\n";
  }
}

void OthelloGame::printBoard() {
  cout << "===0=1=2=3=4=5=6=7===" << endl;
  cout << endl;
  for (int i=0; i<OTHELLO_BOARD_SIZE; i++) {
    cout << i << "  ";
    for (int j=0; j<OTHELLO_BOARD_SIZE; j++) {
      cout << theme[board->get(i,j)] << " ";
    }
    cout << " =\n";
  }
  cout << "=====================" << endl;
}

move_pair_t OthelloGame::convertMove(string move){
  istringstream iss(move);
  int a,b;
  iss >> a >> b;
  return make_pair(a, b);
}


int OthelloGame::countValidMoves(int role) {
  int res = 0;
  for (auto & move : possible_moves) {
    if (isValidMove(move.first, move.second, role)) {
      res += 1;
    }
  }
  return res;
}

int OthelloGame::countCorners(int role) {
  int res = 0;
  if (board->get(0,0) == role) res ++;
  if (board->get(0,7) == role) res ++;
  if (board->get(7,0) == role) res ++;
  if (board->get(7,7) == role) res ++;
  return res;
}

/*
int main() {
  OthelloGame o;
  o.printBoard();  
  cout << "possible moves:\n";
  cout << "  ";
  for (auto & move : o.possible_moves) {
    cout << move.first << '-' << move.second << ',';
  }
  cout << endl;
  printf("winner is %d, player is %d\n", o.getWinner(), o.turn);
  printf("valid moves for first player:\n");
  printf("  ");
  for (auto & move : o.possible_moves) {
    if (o.isValidMove(move)) {
      printf("%d-%d, ", move.first, move.second);
    }
  }

  printf("\n");

  int h1, h2;
  h1 = (o.othello_hash.HASH_1[0][27]
       + o.othello_hash.HASH_1[1][35]
       + o.othello_hash.HASH_1[0][36]
       + o.othello_hash.HASH_1[1][28])
       % HASH_MOD_1;

  h2 = (o.othello_hash.HASH_2[0][27]
     + o.othello_hash.HASH_2[1][35]
     + o.othello_hash.HASH_2[0][36]
     + o.othello_hash.HASH_2[1][28])
     % HASH_MOD_2;
  
  printf("hash 1 should be %d: %d\n", h1, o.hash_key.first);
  printf("hash 2 should be %d: %d\n", h2, o.hash_key.second);

  printf("\n");
  printf("play 5-3\n");

  o.progress(mp(5,3));
  o.printBoard();
  cout << "possible moves:\n";
  cout << "  ";
  for (auto & move : o.possible_moves) {
    cout << move.first << '-' << move.second << ',';
  }
  cout << endl;
  printf("player is %d\n", o.turn);
  printf("valid moves for second player:\n");
  printf("  ");
  for (auto & move : o.possible_moves) {
    if (o.isValidMove(move)) {
      printf("%d-%d, ", move.first, move.second);
    }
  }
  printf("\n");
  o.printResult();

  h1 = (o.othello_hash.HASH_1[0][27]
       + o.othello_hash.HASH_1[0][35]
       + o.othello_hash.HASH_1[0][36]
       + o.othello_hash.HASH_1[0][43]
       + o.othello_hash.HASH_1[1][28])
       % HASH_MOD_1;

  h2 = (o.othello_hash.HASH_2[0][27]
     + o.othello_hash.HASH_2[0][35]
     + o.othello_hash.HASH_2[0][36]
     + o.othello_hash.HASH_2[0][43]
     + o.othello_hash.HASH_2[1][28])
     % HASH_MOD_2;
  
  printf("hash 1 should be %d: %d\n", h1, o.hash_key.first);
  printf("hash 2 should be %d: %d\n", h2, o.hash_key.second);

  printf("\n");
  printf("play 5-4 and 5-5\n");

  o.progress(mp(5,4));
  o.progress(mp(5,5));
  o.printBoard();
  cout << "possible moves:\n";
  cout << "  ";
  for (auto & move : o.possible_moves) {
    cout << move.first << '-' << move.second << ',';
  }
  cout << endl;
  printf("player is %d\n", o.turn);
  printf("valid moves for second player:\n");
  printf("  ");
  for (auto & move : o.possible_moves) {
    if (o.isValidMove(move)) {
      printf("%d-%d, ", move.first, move.second);
    }
  }
  printf("\n");
  o.printResult();
}
*/