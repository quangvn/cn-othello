#ifndef __CN_PLAYER__
#define __CN_PLAYER__

#include "player.h"
#include "othello_game.h"
#include "minimax_node.h"
#include "othello_eval.h"

class CNPlayer : public Player {
public:
  static int node_visited;
  static int node_terminal;
  string LOG_FILE;

  CNPlayer();
  CNPlayer(int depth, othello_eval e);
  move_pair_t selectMove(OthelloGame *game);

  string get_name() {
    return "CN player(" + to_string(search_depth) + ")";
  }

private:
  float negamax(MinimaxNode* node, OthelloGame* game, int depth);
  float computeCN(MinimaxNode* node, float target, int sign);
  float compute_cn_adjustment(MinimaxNode* node);
  float compute_cn_mean_eval(MinimaxNode * node);
  
  int search_depth;
  othello_eval eval_function;
};

#endif