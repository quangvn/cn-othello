#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

#include "global_limits.h"
#include "negamax_player.h"
#include "othello_eval.h"

// #define LOG_FILE "log\\composed_ev_4.txt"

#define sqrt(x) pow(x,0.5)
#define sqr(x) (x*x)

using namespace std;

int NegamaxPlayer::node_visited = 0;
int NegamaxPlayer::node_terminal = 0;

NegamaxPlayer::NegamaxPlayer() {
  search_depth = 4;
  eval_function = OthelloEval::simple_eval;
  LOG_FILE = "log\\ev_4.txt";
}

NegamaxPlayer::NegamaxPlayer(int depth, othello_eval e) {
  search_depth = depth;
  eval_function = e;
  LOG_FILE = "log\\ev_1.txt";
}

move_pair_t NegamaxPlayer::selectMove(OthelloGame *game) {
  node_visited = 0;
  node_terminal = 0;
  OthelloGame::game_inst_created = 0;
  OthelloGame::game_inst_destroyed = 0;

  MinimaxNode::node_inst_reused = 0;

  MinimaxNode* root = MinimaxNode::create(game);

  negamax(root, game->clone(), search_depth);
  cout << "Eval: " << root->get_eval() << endl;
  move_pair_t move = root->getBestMove();

  cout << root->count_best_child << " have the same best value\n";
  cout << "Game instances created: " << OthelloGame::game_inst_created << endl;
  cout << "Game instances destroyed: " << OthelloGame::game_inst_destroyed << endl;
  cout << "Cache's size: " << MinimaxNode::cache.size() << endl;
  cout << "Node instances found on cache: " << MinimaxNode::node_inst_reused << endl;
  cout << "Node visited: " << node_visited << endl;

  ofstream log_file(LOG_FILE, ios::out | ios::app);
  if (game->isFirstPlayerTurn()) {
    log_file << root->get_eval();
//  log_file << root->get_eval() << "," << computeCN(root, root->get_eval(),1) << "," << computeCN(root, root->get_eval(),-1) << endl;
    float sum = 0, a, cn_1, cn_2, tmp;
    cn_2 = computeCN(root, root->get_eval()-500, -1);
    // cout << cn_2 << " ";
    for (float x=-450; x<=500; x+=50) {
      cn_1 = cn_2;
      if (x==0) {
        cn_2 = 1;
      } else {
        cn_2 = computeCN(root, root->get_eval()+x, (x<0) ? -1 : 1);
      }
      a = (cn_2-cn_1)/50; // 50 is step, x2-x1
      tmp = (a != 0) ? (log(cn_2) - log(cn_1))/a : (50/cn_1);
      sum += tmp * (x<=0 ? -1 : 1);
      // cout << cn_2 << "|" << tmp << " ";
    }
    // cout << endl;
    log_file << "," << sum << endl;
  } else {
    // log_file << -root->get_eval() << "," << computeCN(root, root->get_eval()-1,-1) << "," << computeCN(root, root->get_eval()+1,1) << endl;
  }
  log_file.close();
  delete root;

  return move;
}

float NegamaxPlayer::compute_cn_adjustment(MinimaxNode * node) {
  float sum = 0, a, cn_1, cn_2, tmp;
  cn_2 = computeCN(node, node->get_eval()-500, -1);
  for (float x=-450; x<=500; x+=50) {
    cn_1 = cn_2;
    if (x==0) {
      cn_2 = 1;
    } else {
      cn_2 = computeCN(node, node->get_eval()+x, (x<0) ? -1 : 1);
    }
    a = (cn_2-cn_1)/50;
    tmp = (a != 0) ? (log(cn_2) - log(cn_1))/a : (50/cn_1);
    sum += tmp * (x<=0 ? -1 : 1);
  }
  return sum;
}

float NegamaxPlayer::compute_cn_mean_eval(MinimaxNode * node) {
  float sum = 0, sump = 0, a, b, cn_1, cn_2, tmp;
  cn_2 = computeCN(node, node->get_eval()-500, -1);
  float eval = node->get_eval();
  for (float x=-450; x<=500; x+=50) {
    cn_1 = cn_2;
    if (x==0) {
      cn_2 = 1;
    } else {
      cn_2 = computeCN(node, node->get_eval()+x, (x<0) ? -1 : 1);
    }

    a = (cn_2-cn_1)/50;
    b = cn_2 - a*(eval+x);

    tmp = (a!=0) ? (log(cn_2) - log(cn_1))/a : (50/cn_1);
    sump += tmp;

    if (abs(a) >= EPS && abs(b) >= EPS) {
      tmp = 50.0/a + b*(log(cn_1)-log(cn_2))/sqr(a);
    } else{ 
      if (abs(a) < EPS) {
        tmp = (100*(eval+x)-2500)/(2*b);
      } else {
        tmp = 50/a;
      }
    }
//    cout << "[" << eval+x-50 << "," << cn_1 << "] [" << eval+x << "," << cn_2 << "] -> " << tmp << endl;
    sum += tmp;
  }
  return sum/sump;
}

// for comparing moves, highest value first
bool move_compare(const pair<float, move_pair_t>& lx, const pair<float, move_pair_t>& rx) {
  return lx.first > rx.first;
}

int NegamaxPlayer::rank_move(OthelloGame *game) {
  node_visited = 0;
  MinimaxNode* root = MinimaxNode::create(game);

  negamax(root, game->clone(), search_depth);

  ofstream log_file(LOG_FILE, ios::out | ios::app);
  vector<pair<float, move_pair_t> > ranked_moves;

  MinimaxNode* node;
  float adjusted_eval;
  auto children = root->get_children();

  for (auto & child : (*children)) {
    node = child.second;
    // adjusted_eval = node->get_eval();// + compute_cn_adjustment(node);
    adjusted_eval = compute_cn_mean_eval(node);
    ranked_moves.push_back(make_pair(-adjusted_eval, child.first));
    // cout << "(" << node->get_eval() << "|" << adjusted_eval << ")" << " ";
  }
  cout << endl << endl;

  sort(ranked_moves.begin(), ranked_moves.end(), move_compare);
  for (int i=0; i<ranked_moves.size(); i++) {
    log_file << ranked_moves[i].second.first*8 + ranked_moves[i].second.second << ",";
  }

  delete root;
  log_file << endl;
  log_file.close();
  return node_visited;
}

int NegamaxPlayer::rank_move_by_alpha_beta(OthelloGame *game) {
  node_visited = 0;
  MinimaxNode* root = MinimaxNode::create(game);

  // negamax_no_remember(root, game->clone(), search_depth);

  ofstream log_file(LOG_FILE, ios::out | ios::app);
  vector<pair<float, move_pair_t> > ranked_moves;

  OthelloGame * next_game;
  auto children = root->get_children(game);
  for (auto & child : (*children)) {
    next_game = new OthelloGame(game, child.first);
    child.second = MinimaxNode::create(next_game);
    alphabeta(child.second, next_game, -INF, INF, search_depth-1);
    ranked_moves.push_back(make_pair(-child.second->get_eval(), child.first));
  }

  sort(ranked_moves.begin(), ranked_moves.end(), move_compare);
  for (int i=0; i<ranked_moves.size(); i++) {
    log_file << ranked_moves[i].second.first*8 + ranked_moves[i].second.second << ",";
  }
  /*
  log_file << endl;
  for (int i=0; i<ranked_moves.size(); i++) {
    log_file << ranked_moves[i].first << ",";
  }
  */
  delete root;
  log_file << endl;
  log_file.close();
  return node_visited;
}

float NegamaxPlayer::negamax_no_remember(MinimaxNode* node, OthelloGame* game, int depth) {
  node_visited ++;
  if (depth == 0 or game->isEnd()) {
    node->set_eval(eval_function(game, game->turn));
    node_terminal ++;
    delete game;
    return node->get_eval();
  }

  auto children = node->get_children(game);
  float best = -INF;
  float temp;
  OthelloGame * next_game;
  for (auto & child : (*children)) {
    next_game = new OthelloGame(game, child.first);
    child.second = MinimaxNode::create(next_game);
    temp = -negamax_no_remember(child.second, next_game, depth-1);
    if (temp>best) {
      best = temp;
    }
  }

  node->set_eval(best);
  if (depth < search_depth -1) {
    node->remove_children();
  }
  delete game;
  return best;
}

float NegamaxPlayer::negamax(MinimaxNode* node, OthelloGame* game, int depth) {
  node_visited ++;
  if (depth == 0 or game->isEnd()) {
    node->set_eval(eval_function(game, game->turn));
    node_terminal ++;
    delete game;
    return node->get_eval();
  }

  auto children = node->get_children(game);
  float best = -INF;
  float temp;
  OthelloGame * next_game;
  for (auto & child : (*children)) {
    next_game = new OthelloGame(game, child.first);
    child.second = MinimaxNode::create(next_game);
    temp = -negamax(child.second, next_game, depth-1);
    if (temp>best) {
      best = temp;
    }
  }
  node->set_eval(best);
  delete game;
  return best;
}

float NegamaxPlayer::alphabeta(MinimaxNode* node, OthelloGame* game, float alpha, float beta, int depth) {
  node_visited ++;
  if (depth == 0 or game->isEnd()) {
    node->set_eval(eval_function(game, game->turn));
    delete game;
    return node->get_eval();
  }

  auto children = node->get_children(game);
  float temp;
  OthelloGame * next_game;
  for (auto & child : (*children)) {
    next_game = new OthelloGame(game, child.first);
    child.second = MinimaxNode::create(next_game);
    temp = -alphabeta(child.second, next_game, -beta, -alpha, depth-1);
    if (temp >= beta) {
      alpha = beta;
      break;
    }
    if (temp > alpha) {
      alpha = temp;
    }
  }
  node->set_eval(alpha);
  delete game;
  if (depth < search_depth -1) {
    node->remove_children();
  }
  return alpha;
}

float NegamaxPlayer::computeCN(MinimaxNode* node, float target, int sign) {
  if ((node->get_eval() - target)*sign > 0) {
    return 0;
  }
  if (node->is_terminal) {
    return INF;
  }

  auto children = node->get_children();
  // will need to change for hash
  if (children == NULL || children->size() == 0) {
    return 1;
  }

  float res = (sign == 1) ? INF : 0;
  float tmp;
  for (auto & child : (*children)) {
    tmp = computeCN(child.second, -target, -sign);
    res = (sign == 1) ? min(res, tmp) : (res + tmp);
  }

  return res;
}