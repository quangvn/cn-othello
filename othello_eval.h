#ifndef __OTHELLO_EVAL_H__
#define __OTHELLO_EVAL_H__

#include "othello_game.h"

#define CONFIG_FILE "h-coef.txt"

typedef float(*othello_eval)(OthelloGame *game, int role);

class OthelloEval {
public:
  // (1 * p) + (1000 * c) + (-200 * l) + (100 * m) + (-0 * f) + (0 * d)
  // in order: discs, corner, next_to_empty_corner, mobility, frontier_discs, static_position
  static float coef[6];
  static void load_parameter();
  static float simple_eval(OthelloGame *game, int role);
  static float dynamic_heuristic_evaluation_function(OthelloGame *game, int role);
};

#endif