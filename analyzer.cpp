#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <Windows.h>
#include <vector>
#include <cstdio>
#include <cstring>
#include <sstream>

#define sqr(x) (x*x)
using namespace std;

vector<string> get_filenames_in_folder(string folder)
{
  vector<string> names;
  char search_path[200];
  sprintf(search_path, "%s/*.*", folder.c_str());
  WIN32_FIND_DATA fd;
  HANDLE hFind = ::FindFirstFile(search_path, &fd);
  if(hFind != INVALID_HANDLE_VALUE) {
      do {
          // read all (real) files in current folder
          // , delete '!' read other 2 default folder . and ..
          if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {
              names.push_back(fd.cFileName);
          }
      }while(::FindNextFile(hFind, &fd));
      ::FindClose(hFind);
  } 
  return names;
}

double get_similarity(vector<int> l1, vector<int> l2) {
  if (l1.size() != l2.size()) {
    cout << "ERROR: different size!\n";
  }
  double sum = 0;
  int len = l1.size();
  for (int i=0; i<len; i++) {
    for (int j=0; j<len; j++) {
      if (l1[i] == l2[j]) {
        sum += len - abs(i-j);
      }
    }
  }
  sum = sum/sqr(len);
  return sum;
}

int main() {
  vector<string> files = get_filenames_in_folder("log");

  vector<string> cn_files;
  for (auto & file : files) {
    if (file.find("cn") != string::npos) {
      cn_files.push_back(file);
    }
  }

  for (auto & file : cn_files) {
    // cn file
    ifstream cn_file_stream("log\\" + file, std::ifstream::in);
    // ab file
    file.replace(file.find("cn"), 2, "ab");
    ifstream ab_file_stream("log\\" + file, std::ifstream::in);
    // ab7 file
    file.insert(file.find("."), "_7");
    ifstream ab7_file_stream("log\\" + file, std::ifstream::in);

    if (! (cn_file_stream.is_open() && ab_file_stream.is_open())) {
      cout << "Cannot open file: " << file << endl;
      continue;
    }

    vector<vector<int> > cn_moves_list, ab3_moves_list, ab5_moves_list, ab7_moves_list;
    string cn_line, ab3_line, ab5_line, ab7_line;
    int m;
    char c;
    vector<int> tmp;
    while (getline(cn_file_stream, cn_line)) {
      // cn
      istringstream icn(cn_line);
      tmp.clear();
      while (icn >> m) {
        tmp.push_back(m);
        icn >> c;
      }
      cn_moves_list.push_back(tmp);

      // ab3 and ab5
      getline(ab_file_stream, ab5_line);
      getline(ab_file_stream, ab3_line);

      istringstream iab3(ab3_line);
      tmp.clear();
      while (iab3 >> m) {
         tmp.push_back(m);
        iab3 >> c;
      }
      ab3_moves_list.push_back(tmp);

      istringstream iab5(ab5_line);
      tmp.clear();
      while (iab5 >> m) {
        tmp.push_back(m);
        iab5 >> c;
      }
      ab5_moves_list.push_back(tmp);

      // ab7
      getline(ab7_file_stream, ab7_line);
      istringstream iab7(ab7_line);
      tmp.clear();
      while (iab7 >> m) {
        tmp.push_back(m);
        iab7 >> c;
      }
      ab7_moves_list.push_back(tmp);
    }

    // calculate similarity measure
    double sim_cn_ab3, sim_cn_ab5, sim_ab3_ab5, count_exp;
    double sim_cn_ab7, sim_ab3_ab7, sim_ab5_ab7;
    sim_cn_ab3 = 0;
    sim_ab3_ab5 = 0;
    sim_cn_ab5 = 0;
    count_exp = 0;
    sim_cn_ab7 = sim_ab3_ab7 = sim_ab5_ab7 = 0;
    for (int i=0; i<cn_moves_list.size(); i++) {
      //only evaluate those which have sufficient length
      if (cn_moves_list[i].size() >= 5) {
        count_exp ++;
        sim_ab3_ab5 += get_similarity(ab3_moves_list[i], ab5_moves_list[i]);
        sim_cn_ab3 += get_similarity(cn_moves_list[i], ab3_moves_list[i]);
        sim_cn_ab5 += get_similarity(cn_moves_list[i], ab5_moves_list[i]);

        sim_cn_ab7 += get_similarity(cn_moves_list[i], ab7_moves_list[i]);
        sim_ab3_ab7 += get_similarity(ab3_moves_list[i], ab7_moves_list[i]);
        sim_ab5_ab7 += get_similarity(ab5_moves_list[i], ab7_moves_list[i]);
      }
    }

    cout << "similarity CN - AB3:" << sim_cn_ab3/count_exp << endl;
    cout << "similarity CN - AB5:" << sim_cn_ab5/count_exp << endl;
    cout << "similarity AB3 - AB5:" << sim_ab3_ab5/count_exp << endl;
    cout << "similarity CN - AB7:" << sim_cn_ab7/count_exp << endl;
    cout << "similarity AB3 - AB7:" << sim_ab3_ab7/count_exp << endl;
    cout << "similarity AB5 - AB7:" << sim_ab5_ab7/count_exp << endl;

    cout << endl;

    cn_file_stream.close();
    ab_file_stream.close();
  }
}