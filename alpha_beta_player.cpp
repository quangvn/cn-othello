#include <fstream>

#include "global_limits.h"
#include "alpha_beta_player.h"
#include "othello_eval.h"

using namespace std;

int AlphaBetaPlayer::node_visited = 0;

AlphaBetaPlayer::AlphaBetaPlayer() {
  search_depth = 4;
  eval_function = OthelloEval::simple_eval;
  logging = true;
}

AlphaBetaPlayer::AlphaBetaPlayer(int depth, othello_eval e) {
  search_depth = depth;
  eval_function = e;
  logging = false;
}

move_pair_t AlphaBetaPlayer::selectMove(OthelloGame *game) {
  node_visited = 0;
  OthelloGame::game_inst_created = 0;
  OthelloGame::game_inst_destroyed = 0;

  MinimaxNode::node_inst_reused = 0;

  MinimaxNode* root = MinimaxNode::create(game);

  alphabeta(root, game->clone(), -INF, INF, search_depth);
  move_pair_t move = root->getBestMove();
  
  if (logging) {
    cout << "Eval: " << root->get_eval() << endl;
    cout << root->count_best_child << " have the same best value\n";
    cout << "Game instances created: " << OthelloGame::game_inst_created << endl;
    cout << "Game instances destroyed: " << OthelloGame::game_inst_destroyed << endl;
    cout << "Cache's size: " << MinimaxNode::cache.size() << endl;
    cout << "Node instances found on cache: " << MinimaxNode::node_inst_reused << endl;
    cout << "Node visited: " << node_visited << endl;
  }
  
  return move;
}

float AlphaBetaPlayer::alphabeta(MinimaxNode* node, OthelloGame* game, float alpha, float beta, int depth) {
  node_visited ++;
  if (depth == 0 or game->isEnd()) {
    node->set_eval(eval_function(game, game->turn));
    delete game;
    return node->get_eval();
  }

  auto children = node->get_children(game);
  float temp;
  OthelloGame * next_game;
  for (auto & child : (*children)) {
    next_game = new OthelloGame(game, child.first);
    child.second = MinimaxNode::create(next_game);
    temp = -alphabeta(child.second, next_game, -beta, -alpha, depth-1);
    if (temp >= beta) {
      alpha = beta;
      break;
    }
    if (temp > alpha) {
      alpha = temp;
    }
  }
  node->set_eval(alpha);
  delete game;
  if (depth < search_depth -1) {
    node->remove_children();
  }
  return alpha;
}